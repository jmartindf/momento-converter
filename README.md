# Momento Converter #

## Problem ##

Day One can import data from Momento, but sometimes has errors.

http://help.dayoneapp.com/tips-and-tutorials/how-do-i-import-from-momento

The file current looks like this.

    Sunday 13 September 2015 
    ========================
    
    19:02 Entry 1 text.
    
    ---
    
    20:08 entry 2 text
    
    ---
    
    20:41 entry 3 text

According to Day One support, the file should look like this.

    Date: Sunday 13 September 2015 7:02:00pm 
    ========================
    
    Entry 1 text.
    
    Date: Sunday 13 September 2015 8:08:00pm 
    ========================
    
    entry 2 text
    
    Date: Sunday 13 September 2015 8:41:00pm 
    ========================
    
    entry 3 text

## Usage ##

Run `converter.py`, giving it an input file and an output file.

    ./converter.py input.txt output.txt

