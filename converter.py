#!/usr/local/bin/python3
import re, sys
from datetime import datetime

inputFile = ""
outputFile = ""
contents = ""

if len(sys.argv) == 3:
	inputFile = sys.argv[1]
	outputFile = sys.argv[2]
else:
	print("Need to specify both an input file and an output file.")
	sys.exit(0)

dates = re.compile("^(\w+ \d{1,2} \w+ \d{4})+\n[=]{1,35}\n\n", re.M)
times = re.compile("^(\d{2}:\d{2}) ", re.M)
separator = "========================"

with open(inputFile,"r") as file:
	contents = file.read()

results = dates.split(contents)
with open(outputFile,"w") as output:
	for index,date in enumerate(results[1::2]):
		dt = datetime.strptime(date,"%A %d %B %Y")
		date = dt.strftime("%B %d, %Y")
		entries = results[(index+1)*2]
		entriesArray = times.split(entries)
		for entryIndex,entryTime in enumerate(entriesArray[1::2]):
			hour, minutes = entryTime.split(":")
			if int(hour) > 12:
				entryTime = "%s:%s:00 PM CST" % (int(hour)-12,minutes)
			else:
				entryTime = "%s:%s:00 AM CST" % (hour,minutes)
			entry = entriesArray[(entryIndex+1)*2]
			entry = entry.replace("\n\n---\n","")
			# Format as
			# September 24, 2015 at 07:58:00 AM CST
			print("Date: %s at %s\n%s\n\n%s\nSource: Momento Export\n\n" % (date,entryTime,separator,entry),file=output)


